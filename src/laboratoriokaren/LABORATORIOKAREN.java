/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriokaren;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class LABORATORIOKAREN {

    public static Object[][] equipos = new Object[6][5];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //Inicializando los nombres de los equipos en una matriz
        equipos[0][0] = "HEREDIA";
        equipos[1][0] = "SAN CARLOS";
        equipos[2][0] = "CARTAGO";
        equipos[3][0] = "SAPRISA";
        equipos[4][0] = "ALAJUELA";
        equipos[5][0] = "LIMÓN";
        for (int i = 0; i < equipos.length; i++) {
            equipos[i][1] = 0;//goles a favor
            equipos[i][2] = 0;//goles en contra
            equipos[i][3] = 0;//puntos
            equipos[i][4] = 0;//guardar partidos ganados
        }

        //menu de opciones
        Scanner opc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("");
            System.out.println("MENÚ PRINCIPAL");
            System.out.println("");
            System.out.println("1. Jugar");
            System.out.println("2. Tabla de posiciones");
            System.out.println("3. Reporte");
            System.out.println("4. Salir");
            System.out.println("Ingrese la opcion que desea realizar:");
            opcion = opc.nextInt();
            switch (opcion) {

                case 1:
                    jugar.jug();
                    break;
                case 2:
                    tablaposi.tpos();
                    break;
                case 3:
                    reporte.rep();
                    break;
                case 4:
                    salir = true;
                    System.out.println("Opcion de salida");
                    break;
            }

        }
    }
}
