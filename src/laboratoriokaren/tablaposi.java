/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriokaren;

import java.util.Arrays;

/**
 *
 * @author Usuario
 */
public class tablaposi {

    public static void tpos() {
        Equipo[] arrayEquipos = new Equipo[6];
        for (int c = 0; c < LABORATORIOKAREN.equipos.length; c++) {
            arrayEquipos[c] = new Equipo((String) LABORATORIOKAREN.equipos[c][0], (int) LABORATORIOKAREN.equipos[c][1], (int) LABORATORIOKAREN.equipos[c][2], (int) LABORATORIOKAREN.equipos[c][3]);
        }
        Arrays.sort(arrayEquipos);
        System.out.println("____________________");
        System.out.println("| EQUIPO  |GF|GC|PTS");
        System.out.println("--------------------");
        imprimirArrayEquipos(arrayEquipos);

    }

    public class Futbol {

    }

    static class Equipo implements Comparable<Equipo> {

        public String EQUIP;
        public int GF;
        public int GC;
        public int EQ;

        public Equipo(String EQUIP, int GF, int GC, int EQ) {
            this.EQUIP = EQUIP;
            this.GF = GF;
            this.GC = GC;
            this.EQ = EQ;

        }

        @Override
        public int compareTo(Equipo e) {
            Integer a = this.EQ;
            Integer b = e.EQ;
            if (b.compareTo(a) == 0) {
                Integer x = this.GF;
                Integer y = e.GF;
                if (y.compareTo(x) == 0) {
                    Integer l = this.GC;
                    Integer d = e.GC;
                    return l.compareTo(d);

                } else {

                    return y.compareTo(x);

                }

            } else {
                return b.compareTo(a);
            }
        }
    }

    static void imprimirArrayEquipos(Equipo[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(".| " + array[i].EQUIP + " | " + array[i].GF + " | " + array[i].GC + " | " + array[i].EQ);
        }
    }

}
